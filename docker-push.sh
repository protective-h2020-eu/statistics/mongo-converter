#!/bin/bash

set -ex

IMAGE_NAME="protective-h2020-eu/statistics/mongo-converter"

REGISTRY="registry.gitlab.com"

docker push ${REGISTRY}/${IMAGE_NAME}
