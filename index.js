const Binary = require('mongodb').Binary;
const MongoClient = require('mongodb').MongoClient;
const MongoOplog = require("mongo-oplog");
const winston = require('winston');

const dbPort = process.env.DB_PORT.replace("127.0.0.1:", "");
const dbURL = process.env.DB_URL;
const dbName = 'mentat';
const oplog = MongoOplog('mongodb://'+ dbURL + ':' + dbPort + '/local',{ ns: 'mentat.alerts' })
const url = 'mongodb://'+ dbURL + ':' + dbPort;


const epoch_delta = 2208988800;

//configure logger to log to a file
winston.add(winston.transports.File, { filename: 'logs/tail_oplog.log', level: 'debug' });

winston.info("Listening to DB: " + dbURL + " and port: " + dbPort);
oplog.tail();

oplog.on('insert', doc => {
  // Take the event
  var event = doc.o;
  winston.info("----> EVENT DETECTED <----");
  
  for (field in event) {
    winston.debug("Field to analyze: " + field);
    var fieldValue = event[field];
    if (isBinary(fieldValue)) {
      // Take dateField
      var date;
      // First try to get the field from the msg_raw2 (is faster and safer)
      var msgRawJSON = JSON.parse(event.msg_raw2);
      if (msgRawJSON[field] != undefined){
          winston.debug("Field found in msg_raw2: " + field + "value: "+msgRawJSON[field]);
          date = new Date(msgRawJSON[field]);
      }else{
          date = convertDate(fieldValue);
      }

      // Replace the field with the new value
      if (date) event[field] = date;
    }

    // if the field is Source or Target, we navigate inside them to find the IP field
    if (field == "Source" || field == "Target"){
      winston.debug("Source or Target field detected");

      fieldValue.forEach(function (sourceORtargetField, i) {
        var IP4field = sourceORtargetField.IP4;
        if (IP4field != undefined){
            IP4field.forEach(function (ip4, j) {
                var ipBin = ip4.ip;
                var maxBin = ip4.max;
                var minBin = ip4.min;
    
                if (ipBin != undefined){
                  var ip = convertIP(ipBin); 
                  
                  // Replace the fields with the new values
                  event[field][i].IP4[j].ip = ip;
                } 
                if (maxBin != undefined){
                  var max = convertIP(maxBin); 
                  
                  // Replace the fields with the new values
                  event[field][i].IP4[j].max = max;
                } 
                if (minBin != undefined){
                  var min = convertIP(minBin); 
                  // Replace the fields with the new values
                  event[field][i].IP4[j].min = min;
                } 
            });
        }
      });
    }
  }

  // Add modified event to new collection
  add2Collection(event);

});

oplog.stop(() => {
  winston.info('Server waiting for an input...');
});

/**
 * This function checks if a field is in Binary format or not usign the length() function that only is applicable for Binary fields
 * https://mongodb.github.io/node-mongodb-native/api-bson-generated/binary.html
 * @param {field to be checked} fieldValue
 */
function isBinary(fieldValue){
  try {
    fieldValue.length();
    winston.debug ("The field is Binary");
    return true;
  }
  catch(error) {
    winston.debug ("The field is NOT Binary");
    return false;
  }
}

/**
 * This function takes the modified event and store it into the new collection.
 * 
 * @param {*} event [the event to be inserted in the new collection]
 **/
function add2Collection(event){
  MongoClient.connect(url, function(err, client) {
    if (err) throw err;
    const db = client.db(dbName);
    db.collection("alerts_dates").insertOne(event, function(err, res) {
      if (err) throw err;

      winston.info("<---- MODIFIED EVENT INSERTED ---->");

      client.close();
    });
  });
}

/**
 * This function converts from binary to Date following CESNET's method
 * 
 * @param {*} dateField  [the binary field to be converted to Date format]
 */
function convertDate(dateField){
  //Split and take only the desired parts
  var highInt = dateField.buffer.readUIntBE(0, 4);
  var lowInt = dateField.buffer.readUIntBE(4, 4);

  var stamp = highInt - epoch_delta + (lowInt)*0/(2 ^ 32);

  var date = new Date(stamp *1000);
    
  winston.debug("Date parsed: " + date);

  return date;
}

/**
 * This function converts from binary to Date following CESNET's method
 * 
 * @param {*} dateField [the binary field to be converted to Date format] 
 */
function convertIP(ipBin){
  
  var ipInt = ipBin.buffer.readUIntBE(0, 4);
  var ip = toIP(ipInt);
  winston.debug("IP4 parsed: " + ip);

  return ip;
}

/**
 * Parses Integer to IPv4
 *
 * @param  {String} value [value to parse]
 * @return {String}       [IPv4 String of value provided]
 */
function toIP(value) {

  const result = /\d+/.exec(value);

  value = result[0];

  return [
    (value>>24)&0xff,
    (value>>16)&0xff,
    (value>>8)&0xff,
    value&0xff
  ].join('.');
}

